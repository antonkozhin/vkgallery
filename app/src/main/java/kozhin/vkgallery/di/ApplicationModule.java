package kozhin.vkgallery.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kozhin.vkgallery.Application;
import kozhin.vkgallery.utils.image.ImageDownloader;
import kozhin.vkgallery.utils.image.LocalCache;

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    public LocalCache cache() {
        return new LocalCache(provideContext());
    }

    @Provides
    @Singleton
    public ImageDownloader imageDownloader() {
        return new ImageDownloader(cache());
    }

}
