package kozhin.vkgallery.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import kozhin.vkgallery.grid.di.PhotoGridComponent;
import kozhin.vkgallery.grid.di.PhotoGridModule;
import kozhin.vkgallery.utils.image.ImageDownloader;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    Context provideContext();

    ImageDownloader imageDownloader();

    PhotoGridComponent photoGridComponent(PhotoGridModule photoGridModule);
}
