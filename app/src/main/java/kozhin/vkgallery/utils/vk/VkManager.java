package kozhin.vkgallery.utils.vk;

public interface VkManager  {

    interface VkManagerCallback {

        void sessionAwakened(LoginState state);
        void onResult();

    }

    enum LoginState {
        Unknown,
        LoggedOut,
        Pending,
        LoggedIn
    }

    void setManagerCallback(VkManagerCallback managerCallback);
    void login();
    void logout();
    boolean isLoggedIn();
    void wakeUpSession();
    boolean onActivityResult(int requestCode, int resultCode);

}
