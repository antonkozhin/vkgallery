package kozhin.vkgallery.utils.vk;

import android.app.Activity;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

public class VkManagerImp implements VkManager {

    private static final String[] scopes = new String[]{VKScope.PHOTOS};

    private Activity activity;
    private VkManagerCallback managerCallback;

    public VkManagerImp(Activity activity) {
        this.activity = activity;
    }

    public void setManagerCallback(VkManagerCallback managerCallback) {
        this.managerCallback = managerCallback;
    }

    @Override
    public void login() {
        VKSdk.login(activity, scopes);
    }

    @Override
    public void logout() {
        VKSdk.logout();
    }

    @Override
    public boolean isLoggedIn() {
        return VKSdk.isLoggedIn();
    }

    @Override
    public void wakeUpSession() {
        VKSdk.wakeUpSession(activity, new VKCallback<VKSdk.LoginState>() {
            @Override
            public void onResult(VKSdk.LoginState res) {
                    switch (res) {
                        case LoggedOut:
                            managerCallback.sessionAwakened(LoginState.LoggedOut);
                            break;
                        case LoggedIn:
                            managerCallback.sessionAwakened(LoginState.LoggedIn);
                            break;
                        default:
                            break;
                }
            }

            @Override
            public void onError(VKError error) {

            }
        });
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode) {
        final VKCallback<VKAccessToken> callback = new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                managerCallback.onResult();
            }

            @Override
            public void onError(VKError error) {
            }
        };
        return VKSdk.onActivityResult(requestCode, resultCode, activity.getIntent(), callback);
    }

}
