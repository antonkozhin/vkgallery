package kozhin.vkgallery.utils.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

public class ImageDownloader {

    private LocalCache cache;
    private ExecutorService executorService;
    private static final int N_THREADS = 30;

    @Inject
    public ImageDownloader(LocalCache cache) {
        this.cache = cache;
        executorService = Executors.newFixedThreadPool(N_THREADS);
    }

    public void download(String url, String id, ImageView imageView, ProgressBar progressBar) {
        if (cancelPotentialDownload(url, imageView)) {
            BitmapDownloaderTask task = new BitmapDownloaderTask(imageView, progressBar);
            DownloadedDrawable downloadedDrawable = new DownloadedDrawable(task);
            imageView.setImageDrawable(downloadedDrawable);
            task.executeOnExecutor(executorService, url, id);
        }
    }

    private static boolean cancelPotentialDownload(String url, ImageView imageView) {
        BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
        if (bitmapDownloaderTask != null) {
            String bitmapUrl = bitmapDownloaderTask.url;
            if ((bitmapUrl == null) || (!bitmapUrl.equals(url))) {
                bitmapDownloaderTask.cancel(true);
            } else {
                return false;
            }
        }
        return true;
    }

    public static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadedDrawable) {
                DownloadedDrawable downloadedDrawable = (DownloadedDrawable)drawable;
                return downloadedDrawable.getBitmapDownloaderTask();
            }
        }
        return null;
    }

    public class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {

        String url;
        final WeakReference<ImageView> imageViewReference;
        final WeakReference<ProgressBar> progressBarReference;

        BitmapDownloaderTask(ImageView imageView, ProgressBar progressBar) {
            this.imageViewReference = new WeakReference<>(imageView);
            this.progressBarReference = new WeakReference<>(progressBar);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressBarReference.get().setVisibility(View.VISIBLE);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            if (isCancelled()) {
                return null;
            }
            url = params[0];
            final String id = params[1];

            Bitmap bitmap = cache.getBitmapFromCache(id);
            // Not found in cache
            if (bitmap == null) {
                bitmap = downloadBitmap(url);
                // Add bitmap to cache
                cache.addBitmapToCache(id, bitmap);
            }

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }

            final ProgressBar progressBar = progressBarReference.get();
            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }

            final ImageView imageView = imageViewReference.get();
            BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
            if (this == bitmapDownloaderTask) {
                AlphaAnimation fadeInAnimation = new AlphaAnimation(0, 1);
                fadeInAnimation.setInterpolator(new AccelerateInterpolator());
                fadeInAnimation.setDuration(500);
                imageView.setVisibility(View.GONE);
                imageView.setImageBitmap(bitmap);
                fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {

                    @Override
                    public void onAnimationStart(Animation animation) {}

                    @Override
                    public void onAnimationRepeat(Animation animation) {}

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        imageView.setVisibility(View.VISIBLE);
                    }
                });
                imageView.startAnimation(fadeInAnimation);
            }
        }

        private Bitmap downloadBitmap(String bitmapUrl) {
            try {
                URL url = new URL(bitmapUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = null;
                try {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    return BitmapFactory.decodeStream(inputStream);
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}