package kozhin.vkgallery.utils.image;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import java.lang.ref.WeakReference;

class DownloadedDrawable extends ColorDrawable {

    private final WeakReference<ImageDownloader.BitmapDownloaderTask> bitmapDownloaderTaskReference;

    DownloadedDrawable(ImageDownloader.BitmapDownloaderTask bitmapDownloaderTask) {
        super(Color.TRANSPARENT);
        this.bitmapDownloaderTaskReference = new WeakReference<>(bitmapDownloaderTask);
    }

    public ImageDownloader.BitmapDownloaderTask getBitmapDownloaderTask() {
        return bitmapDownloaderTaskReference.get();
    }
}
