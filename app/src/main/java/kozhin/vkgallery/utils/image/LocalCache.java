package kozhin.vkgallery.utils.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.util.LruCache;

import com.jakewharton.disklrucache.DiskLruCache;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class LocalCache {

    private static final int DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10MB
    private static final String DISK_CACHE_SUBDIR = "thumbnails";

    private LruCache<String, Bitmap> memoryCache;
    private DiskLruCache diskLruCache;

    public LocalCache(Context context) {
        initMemoryCache();
        initDiskCache(context);
    }

    public void addBitmapToCache(String key, Bitmap bitmap) {
        if (key != null && bitmap != null) {
            addBitmapToMemoryCache(key, bitmap);
            addBitmapToDiskCache(key, bitmap);
        }
    }

    public Bitmap getBitmapFromCache(String key) {
        Bitmap bitmap = getBitmapFromMemCache(key);
        if (bitmap == null) {
            bitmap = getBitmapFromDiskCache(key);
        }
        return bitmap;
    }

    private void initMemoryCache() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        memoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return (bitmap.getRowBytes() * bitmap.getHeight()) / 1024;
            }
        };
    }

    private void initDiskCache(Context context) {
        new InitDiskCacheTask().execute(getDiskCacheDir(context, DISK_CACHE_SUBDIR));
    }

    private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            memoryCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromMemCache(String key) {
        return memoryCache.get(key);
    }

    private void addBitmapToDiskCache(String key, Bitmap bitmap) {
        if (diskLruCache != null) {
            DiskLruCache.Editor editor = null;
            try {
                editor = diskLruCache.edit(key);
                if (editor == null) {
                    return;
                }
                if (writeBitmapToFile(bitmap, editor)) {
                    diskLruCache.flush();
                    editor.commit();
                } else {
                    editor.abort();
                }
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    if (editor != null) {
                        editor.abort();
                    }
                } catch (IOException ioe) {
                    // do nothing
                }
            }
        }
    }

    private Bitmap getBitmapFromDiskCache(String key) {
        Bitmap bitmap = null;
        if (diskLruCache != null) {
            DiskLruCache.Snapshot snapshot = null;
            try {
                snapshot = diskLruCache.get(key);
                if (snapshot == null) {
                    return null;
                }
                final InputStream is = snapshot.getInputStream(0);
                if (is != null) {
                    final BufferedInputStream bis = new BufferedInputStream(is);
                    bitmap = BitmapFactory.decodeStream(bis);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (snapshot != null) {
                    snapshot.close();
                }
            }
        }
        return bitmap;
    }

    private boolean writeBitmapToFile(Bitmap bitmap, DiskLruCache.Editor editor) throws IOException {
        OutputStream os = null;
        try {
            os = new BufferedOutputStream(editor.newOutputStream(0));
            return bitmap.compress(CompressFormat.JPEG, 70, os);
        } finally {
            if (os != null) {
                os.close();
            }
        }
    }

    private static File getDiskCacheDir(Context context, String uniqueName) {
        final String cachePath =
                Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ? context.getExternalCacheDir().getPath() :
                        context.getCacheDir().getPath();
        return new File(cachePath + File.separator + uniqueName);
    }

    class InitDiskCacheTask extends AsyncTask<File, Void, Void> {
        @Override
        protected Void doInBackground(File... params) {
            File cacheDir = params[0];
            try {
                diskLruCache = DiskLruCache.open(cacheDir, 1, 1, DISK_CACHE_SIZE);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

}