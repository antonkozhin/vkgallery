package kozhin.vkgallery.main.presenter;

import javax.inject.Inject;

import kozhin.vkgallery.main.contract.MainContract;
import kozhin.vkgallery.utils.vk.VkManager;

public class MainPresenter implements MainContract.Presenter, VkManager.VkManagerCallback {

    private MainContract.Navigator navigator;
    private VkManager vkManager;

    private boolean isResumed = false;

    @Inject
    public MainPresenter(MainContract.Navigator navigator, VkManager vkManager) {
        this.navigator = navigator;
        this.vkManager = vkManager;
        vkManager.setManagerCallback(this);
    }

    @Override
    public void onCreate() {
        vkManager.wakeUpSession();
    }

    @Override
    public void onResume() {
        isResumed = true;
        if (vkManager.isLoggedIn()) {
            navigator.showLogout();
        } else {
            navigator.showLogin();
        }
    }

    @Override
    public void onPause() {
        isResumed = false;
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode) {
        return vkManager.onActivityResult(requestCode, resultCode);
    }

    @Override
    public void login() {
        vkManager.login();
    }

    @Override
    public void logout() {
        vkManager.logout();
        if (vkManager.isLoggedIn()) {
            navigator.showLogin();
        }
    }


    @Override
    public void show() {
        navigator.showPhotos();
    }

    @Override
    public  void sessionAwakened(VkManager.LoginState state) {
        if (isResumed) {
            switch (state) {
                case LoggedOut:
                    navigator.showLogin();
                    break;
                case LoggedIn:
                    navigator.showLogout();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onResult() {
        navigator.showPhotos();
    }

}
