package kozhin.vkgallery.main.contract;

public interface MainContract {

    interface View {

    }

    interface Presenter {

        void onCreate();
        void onResume();
        void onPause();
        boolean onActivityResult(int requestCode, int resultCode);
        void login();
        void logout();
        void show();

    }

    interface Navigator {

        void showLogin();
        void showLogout();
        void showPhotos();
    }

}
