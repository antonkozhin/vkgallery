package kozhin.vkgallery.main.view;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import javax.inject.Inject;

import kozhin.vkgallery.R;
import kozhin.vkgallery.main.contract.MainContract;
import kozhin.vkgallery.main.di.DaggerMainComponent;
import kozhin.vkgallery.main.di.MainComponent;
import kozhin.vkgallery.main.di.MainModule;

public class MainActivity extends FragmentActivity {

    @Inject
    MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMainComponent().inject(this);
        setContentView(R.layout.activity_login);
        presenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        presenter.onPause();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!presenter.onActivityResult(requestCode, resultCode)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private MainComponent getMainComponent() {
        return DaggerMainComponent
                .builder()
                .mainModule(new MainModule(this))
                .build();
    }


}