package kozhin.vkgallery.main.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import javax.inject.Inject;

import kozhin.vkgallery.R;
import kozhin.vkgallery.main.contract.MainContract;
import kozhin.vkgallery.main.di.DaggerMainComponent;
import kozhin.vkgallery.main.di.MainComponent;
import kozhin.vkgallery.main.di.MainModule;

public class ActionsFragment extends android.support.v4.app.Fragment {

    @Inject
    MainContract.Presenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLoginComponent().inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_logout, container, false);
        view.findViewById(R.id.continue_button).setOnClickListener((View v) -> presenter.show());
        view.findViewById(R.id.logout).setOnClickListener((View v) -> presenter.logout());
        return view;
    }

    private MainComponent getLoginComponent() {
        return DaggerMainComponent
                .builder()
                .mainModule(new MainModule(getActivity()))
                .build();
    }
}