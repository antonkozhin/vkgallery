package kozhin.vkgallery.main.di;

import android.support.v4.app.FragmentActivity;

import dagger.Module;
import dagger.Provides;
import kozhin.vkgallery.main.contract.MainContract;
import kozhin.vkgallery.main.navigation.MainNavigator;
import kozhin.vkgallery.main.presenter.MainPresenter;
import kozhin.vkgallery.utils.vk.VkManager;
import kozhin.vkgallery.utils.vk.VkManagerImp;

@Module
public class MainModule {

    private FragmentActivity activity;

    public MainModule(FragmentActivity activity) {
        this.activity = activity;
    }

    @Provides
    @MainScope
    public MainContract.Navigator navigator() {
        return new MainNavigator(activity);
    }

    @Provides
    @MainScope
    public MainContract.Presenter presenter() {
        return new MainPresenter(navigator(), vkManager());
    }

    @Provides
    @MainScope
    public VkManager vkManager() {
        return new VkManagerImp(activity);
    }

}
