package kozhin.vkgallery.main.di;

import dagger.Component;
import kozhin.vkgallery.main.contract.MainContract;
import kozhin.vkgallery.main.view.ActionsFragment;
import kozhin.vkgallery.main.view.MainActivity;
import kozhin.vkgallery.main.view.LoginFragment;
import kozhin.vkgallery.utils.vk.VkManager;

@MainScope
@Component(modules = {MainModule.class})
public interface MainComponent {

    void inject(MainActivity activity);

    void inject(LoginFragment loginFragment);

    void inject(ActionsFragment actionsFragment);

    void inject(MainContract.Presenter presenter);

    MainContract.Presenter presenter();

    MainContract.Navigator navigator();

    VkManager vkManager();
}
