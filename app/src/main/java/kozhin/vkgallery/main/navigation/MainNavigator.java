package kozhin.vkgallery.main.navigation;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;

import kozhin.vkgallery.R;
import kozhin.vkgallery.grid.view.PhotoGridActivity;
import kozhin.vkgallery.main.contract.MainContract;
import kozhin.vkgallery.main.view.LoginFragment;
import kozhin.vkgallery.main.view.ActionsFragment;

public class MainNavigator implements MainContract.Navigator {

    private FragmentActivity activity;

    public MainNavigator(FragmentActivity acitivity) {
        this.activity = acitivity;
    }

    @Override
    public void showLogout() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new ActionsFragment())
                .commitAllowingStateLoss();
    }

    @Override
    public void showLogin() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new LoginFragment())
                .commitAllowingStateLoss();
    }

    @Override
    public void showPhotos() {
        activity.startActivity(new Intent(activity, PhotoGridActivity.class));
    }

}
