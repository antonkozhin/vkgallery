package kozhin.vkgallery.grid.navigator;

import android.support.v4.app.FragmentActivity;

import javax.inject.Inject;

import kozhin.vkgallery.grid.contract.PhotoGridContract;

public class PhotoGridNavigator implements PhotoGridContract.Navigator {

    private FragmentActivity activity;

    @Inject
    public PhotoGridNavigator(FragmentActivity acitivity) {
        this.activity = acitivity;
    }

    public void showPhoto() {

    }

}
