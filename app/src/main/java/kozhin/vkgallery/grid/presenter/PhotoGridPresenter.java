package kozhin.vkgallery.grid.presenter;

import android.support.annotation.NonNull;

import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKRequest.VKRequestListener;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiPhoto;

import org.json.JSONArray;

import java.lang.ref.WeakReference;
import java.util.Arrays;

import javax.inject.Inject;

import kozhin.vkgallery.grid.contract.PhotoGridContract;

public class PhotoGridPresenter implements PhotoGridContract.Presenter {

    private WeakReference<PhotoGridContract.View> view;
    private PhotoGridContract.Navigator navigator;

    private VKRequest request;

    @Inject
    public PhotoGridPresenter(PhotoGridContract.Navigator navigator) {
        this.navigator = navigator;
    }

    @Override
    public void onCreate() {
        request = new VKRequest("photos.getAll" );
        request.executeWithListener(new VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                try {
                    JSONArray jsonArray = response.json.getJSONObject("response").getJSONArray("items");
                    int length = jsonArray.length();
                    final VKApiPhoto[] vkApiPhotos = new VKApiPhoto[length];
                    for (int i = 0; i < length; i++) {
                        vkApiPhotos[i] = new VKApiPhoto(jsonArray.getJSONObject(i));
                    }
                    view.get().setPhotos(Arrays.asList(vkApiPhotos));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        request.cancel();
    }

    @Override
    public void setView(@NonNull PhotoGridContract.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void clickPhoto(int position) {
        //final VKApiPhoto photo = adapter.getItem(position);
        navigator.showPhoto();
    }

}