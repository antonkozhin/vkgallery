package kozhin.vkgallery.grid.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.vk.sdk.api.model.VKApiPhoto;

import javax.inject.Inject;

import kozhin.vkgallery.R;
import kozhin.vkgallery.utils.image.ImageDownloader;

public class PhotoGridAdapter extends ArrayAdapter<VKApiPhoto> {

    private LayoutInflater inflater;
    private ImageDownloader imageDownloader;

    @Inject
    public PhotoGridAdapter(Context context, ImageDownloader imageDownloader) {
        super(context, 0);
        this.inflater = LayoutInflater.from(context);
        this.imageDownloader = imageDownloader;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final VKApiPhoto item = getItem(position);
        View rootView = inflater.inflate(R.layout.grid_item_photo, parent, false);
        ImageView ivPhotoThumb = rootView.findViewById(R.id.photoThumb);
        ProgressBar progressBar = rootView.findViewById(R.id.loading);
        imageDownloader.download(item.photo_604, String.valueOf(item.getId()) + "_normal", ivPhotoThumb, progressBar);

        return rootView;
    }

}