package kozhin.vkgallery.grid.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.vk.sdk.api.model.VKApiPhoto;
import java.util.List;

import javax.inject.Inject;

import kozhin.vkgallery.Application;
import kozhin.vkgallery.R;
import kozhin.vkgallery.grid.contract.PhotoGridContract;
import kozhin.vkgallery.grid.di.PhotoGridComponent;
import kozhin.vkgallery.grid.di.PhotoGridModule;
import kozhin.vkgallery.utils.image.ImageDownloader;

public class PhotoGridActivity extends AppCompatActivity implements PhotoGridContract.View {

    @Inject
    PhotoGridAdapter adapter;

    @Inject
    PhotoGridContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPhotoGridComponent().inject(this);
        setContentView(R.layout.activity_photo_grid);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        final GridView gridView  = findViewById(R.id.photos);
        final TextView emptyView = findViewById(R.id.empty);
        gridView.setEmptyView(emptyView);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener((AdapterView<?> adapterView, View view, int position, long id) -> presenter.clickPhoto(position));
        gridView.setRecyclerListener((View v) -> {
                ImageView ivPhotoThumb = v.findViewById(R.id.photoThumb);
                ImageDownloader.BitmapDownloaderTask bitmapDownloaderTask = ImageDownloader.getBitmapDownloaderTask(ivPhotoThumb);
                if (bitmapDownloaderTask != null) {
                    bitmapDownloaderTask.cancel(true);
                }
        });
        presenter.setView(this);
        presenter.onCreate();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void setPhotos(List<VKApiPhoto> photos) {
        if (photos != null) {
            adapter.clear();
            for (VKApiPhoto photo : photos) {
                adapter.add(photo);
            }
            adapter.notifyDataSetChanged();
        }
    }

    private PhotoGridComponent getPhotoGridComponent() {
        return Application.getApplicationComponent()
                .photoGridComponent(new PhotoGridModule(this));
    }

}