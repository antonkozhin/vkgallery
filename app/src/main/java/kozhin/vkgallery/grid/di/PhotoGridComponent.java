package kozhin.vkgallery.grid.di;

import dagger.Subcomponent;
import kozhin.vkgallery.grid.contract.PhotoGridContract;
import kozhin.vkgallery.grid.view.PhotoGridActivity;
import kozhin.vkgallery.grid.view.PhotoGridAdapter;

@PhotoGridScope
@Subcomponent(modules = {PhotoGridModule.class})
public interface PhotoGridComponent {

    void inject(PhotoGridActivity photoGridActivity);

    PhotoGridContract.Navigator navigator();

    PhotoGridContract.Presenter presenter();

    PhotoGridAdapter adapter();
}
