package kozhin.vkgallery.grid.di;

import android.support.v4.app.FragmentActivity;

import dagger.Module;
import dagger.Provides;
import kozhin.vkgallery.grid.contract.PhotoGridContract;
import kozhin.vkgallery.grid.navigator.PhotoGridNavigator;
import kozhin.vkgallery.grid.presenter.PhotoGridPresenter;
import kozhin.vkgallery.grid.view.PhotoGridAdapter;
import kozhin.vkgallery.utils.image.ImageDownloader;

@Module
public class PhotoGridModule {

    private FragmentActivity activity;

    public PhotoGridModule(FragmentActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PhotoGridScope
    public PhotoGridContract.Navigator navigator() {
        return new PhotoGridNavigator(activity);
    }

    @Provides
    @PhotoGridScope
    public PhotoGridContract.Presenter presenter() {
        return new PhotoGridPresenter(navigator());
    }

    @Provides
    @PhotoGridScope
    public PhotoGridAdapter adapter(ImageDownloader imageDownloader) {
        return new PhotoGridAdapter(activity, imageDownloader);
    }

}
