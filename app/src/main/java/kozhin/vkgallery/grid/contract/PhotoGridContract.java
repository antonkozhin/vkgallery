package kozhin.vkgallery.grid.contract;

import android.support.annotation.NonNull;

import com.vk.sdk.api.model.VKApiPhoto;

import java.util.List;

public interface PhotoGridContract {

    interface View {

        void setPhotos(List<VKApiPhoto> photos);

    }

    interface Presenter {

        void onCreate();
        void onDestroy();
        void setView(@NonNull PhotoGridContract.View view);
        void clickPhoto(int position);

    }

    interface Navigator {

        void showPhoto();

    }

}